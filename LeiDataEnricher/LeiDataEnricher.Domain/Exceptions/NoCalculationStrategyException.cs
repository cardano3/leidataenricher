﻿namespace LeiDataEnricher.Domain.Exceptions
{
    public class NoCalculationStrategyException : Exception
    {
        public NoCalculationStrategyException()
        {
        }

        public NoCalculationStrategyException(string? message) : base(message)
        {
        }
    }
}