﻿namespace LeiDataEnricher.Domain.Exceptions
{
    public class LeiInputDataReaderException : Exception
    {
        public LeiInputDataReaderException()
        {
        }

        public LeiInputDataReaderException(string? message) : base(message)
        {
        }
    }
}