﻿namespace LeiDataEnricher.Domain.Exceptions
{
    public class GleifDataProviderException : Exception
    {
        public GleifDataProviderException() : base()
        {
        }

        public GleifDataProviderException(string message) : base(message)
        {
        }
    }
}