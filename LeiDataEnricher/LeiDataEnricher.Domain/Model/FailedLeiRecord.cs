﻿namespace LeiDataEnricher.Domain.Model
{
    public class FailedLeiRecord
    {
        public string RecordError { get; set; }
    }
}