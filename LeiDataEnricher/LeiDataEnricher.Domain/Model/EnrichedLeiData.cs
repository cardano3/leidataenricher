﻿namespace LeiDataEnricher.Domain.Model
{
    public class EnrichedLeiData : LeiData
    {
        public string LegalName { get; set; }

        public string[] Bic { get; set; }

        public decimal TransactionCosts { get; set; }
    }
}