﻿namespace LeiDataEnricher.Domain.Model
{

    public class LeiData
    {
        public string TransactionUti { get; set; }

        public string Isin { get; set; }

        public decimal Notional { get; set; }

        public string Currency { get; set; }

        public string TransactionType { get; set; }

        public DateTime TransactionDateTime { get; set; }

        public decimal Rate { get; set; }

        public string Lei { get; set; }
    }
}