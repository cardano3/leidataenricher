﻿namespace LeiDataEnricher.Domain.Model
{
    public class EnrichedLeiRecords
    {
        public List<EnrichedLeiData> SuccessfulRecords { get; set; } = new List<EnrichedLeiData>();

        public List<FailedLeiRecord> FailedRecords { get; set; } = new List<FailedLeiRecord>();
    }
}