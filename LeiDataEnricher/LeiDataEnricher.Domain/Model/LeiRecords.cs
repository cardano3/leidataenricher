﻿namespace LeiDataEnricher.Domain.Model
{
    public class LeiRecords
    {
        public List<LeiData> SuccessfulRecords { get; set; } = new List<LeiData>();

        public List<FailedLeiRecord> FailedRecords { get; set; } = new List<FailedLeiRecord>();
    }
}