﻿namespace LeiDataEnricher.Domain.Model
{
    public partial class LeiProviderMetadata
    {
        public LeiMetadata[] Data { get; set; }
    }

    public partial class LeiMetadata
    {
        public string Type { get; set; }

        public string Id { get; set; }

        public Attributes Attributes { get; set; }
    }

    public partial class Attributes
    {
        public string Lei { get; set; }

        public LegalEntity Entity { get; set; }

        public string[] Bic { get; set; }
    }

    public partial class LegalEntity
    {
        public LegalName LegalName { get; set; }

        public Address LegalAddress { get; set; }
    }

    public partial class Address
    {
        public string Country { get; set; }
    }

    public partial class LegalName
    {
        public string Name { get; set; }
    }
}