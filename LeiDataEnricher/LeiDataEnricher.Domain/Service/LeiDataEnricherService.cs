﻿using LeiDataEnricher.Domain.Exceptions;
using LeiDataEnricher.Domain.Interface;
using LeiDataEnricher.Domain.Model;
using log4net;

namespace LeiDataEnricher.Domain.Service
{
    public class LeiDataEnricherService : ILeiDataEnricherService
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(LeiDataEnricherService));

        private readonly ILeiDataProvider _dataProvider;
        private readonly ILeiInputReader _inputReader;
        private readonly IEnrichedLeiDataFactory _enrichedLeiDataFactory;

        public LeiDataEnricherService(ILeiDataProvider dataProvider, ILeiInputReader inputReader, IEnrichedLeiDataFactory enrichedLeiDataFactory)
        {
            _dataProvider = dataProvider;
            _inputReader = inputReader;
            _enrichedLeiDataFactory = enrichedLeiDataFactory;
        }

        public async Task<EnrichedLeiRecords> EnrichLeiDataAsync()
        {
            var result = new EnrichedLeiRecords();
            try
            {
                var inputData = _inputReader.ReadLeiData();
                if (inputData.FailedRecords.Any())
                {
                    result.FailedRecords.AddRange(inputData.FailedRecords);
                }

                var successfullyImportedLeis = SuccessfullyImportedLeis(inputData);

                var leisMetadata = await _dataProvider.GetLeiMetadataAsync(successfullyImportedLeis);

                foreach (var missingLei in MissingLeisMetadata(successfullyImportedLeis, leisMetadata))
                {
                    result.FailedRecords.Add(new FailedLeiRecord { RecordError = $"Lei [{missingLei}] metadata not found." });
                }

                return EnrichInputLeis(result, inputData, leisMetadata);
            }
            catch (LeiInputDataReaderException ex)
            {
                result.FailedRecords.Add(new FailedLeiRecord { RecordError = ex.Message });
            }
            catch (GleifDataProviderException ex)
            {
                result.FailedRecords.Add(new FailedLeiRecord { RecordError = ex.Message });
            }
            catch (Exception ex)
            {
                Log.Error("There was a problem enriching lei records.", ex);
                result.FailedRecords.Add(new FailedLeiRecord { RecordError = "Unhandled error." });
            }

            return result;
        }    

        private EnrichedLeiRecords EnrichInputLeis(EnrichedLeiRecords result, LeiRecords inputData, LeiProviderMetadata leisMetadata)
        {
            foreach (var record in inputData.SuccessfulRecords)
            {
                try
                {
                    var metadata = leisMetadata.Data.FirstOrDefault(lm => lm.Id.Equals(record.Lei));
                    if (metadata != null)
                    {
                        var enrichedRecord = _enrichedLeiDataFactory.BuildEnrichedLeiData(record, metadata);
                        result.SuccessfulRecords.Add(enrichedRecord);
                    }
                }
                catch (NoCalculationStrategyException)
                {
                    result.FailedRecords.Add(new FailedLeiRecord { RecordError = $"Can't calculate transaction costs for lei [{record.Lei}]" });
                    continue;
                }
            }

            return result;
        }

        private static IEnumerable<string> MissingLeisMetadata(string[] successfullyImportedLeis, LeiProviderMetadata leisMetadata) =>
            successfullyImportedLeis.Where(lei =>
                !leisMetadata.Data.Any(ld => ld.Id.Equals(lei)));

        private static string[] SuccessfullyImportedLeis(LeiRecords inputData) =>
            inputData.SuccessfulRecords
                     .Where(r => !string.IsNullOrWhiteSpace(r.Lei))
                     .Select(r => r.Lei)
                     .Distinct()
                     .ToArray();
    }
}