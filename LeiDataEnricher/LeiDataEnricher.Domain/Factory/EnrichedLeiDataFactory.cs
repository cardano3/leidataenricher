﻿using LeiDataEnricher.Domain.Interface;
using LeiDataEnricher.Domain.Model;

namespace LeiDataEnricher.Domain.Factory
{
    public class EnrichedLeiDataFactory : IEnrichedLeiDataFactory
    {
        private readonly IGetTransactionCostsCalculatorStrategy _costsCalculatorStrategy;

        public EnrichedLeiDataFactory(IGetTransactionCostsCalculatorStrategy costsCalculatorStrategy)
        {
            _costsCalculatorStrategy = costsCalculatorStrategy;
        }

        public EnrichedLeiData BuildEnrichedLeiData(LeiData record, LeiMetadata? metadata)
        {
            var country = metadata?.Attributes.Entity.LegalAddress.Country;
            var calculator = _costsCalculatorStrategy.GetStrategy(country);

            return new EnrichedLeiData
            {
                TransactionCosts = calculator.Calculate(record.Notional, record.Rate),
                TransactionUti = record.TransactionUti,
                Isin = record.Isin,
                Notional = record.Notional,
                Currency = record.Currency,
                TransactionType = record.TransactionType,
                TransactionDateTime = record.TransactionDateTime,
                Rate = record.Rate,
                Lei = record.Lei,
                LegalName = metadata.Attributes?.Entity?.LegalName?.Name,
                Bic = metadata.Attributes?.Bic
            };
        }
    }
}