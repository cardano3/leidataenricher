﻿using LeiDataEnricher.Domain.Interface;

namespace LeiDataEnricher.Domain.Strategy
{
    public class NLTransactionCostsCalculator : ITransactionCostsCalculatorStrategy
    {
        public decimal Calculate(decimal notional, decimal rate) =>
            Math.Abs(notional * (1 / rate) - notional);
    }
}