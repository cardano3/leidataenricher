﻿using LeiDataEnricher.Domain.Exceptions;
using LeiDataEnricher.Domain.Interface;

namespace LeiDataEnricher.Domain.Strategy
{
    public class GetTransactionCostsCalculatorStragety : IGetTransactionCostsCalculatorStrategy
    {
        public ITransactionCostsCalculatorStrategy GetStrategy(string countryCode)
        {
            switch (countryCode?.ToLower())
            {
                case "gb":
                    return new GBTransactionCostsCalculator();
                case "nl":
                    return new NLTransactionCostsCalculator();
                default:
                    throw new NoCalculationStrategyException($"No strategy implemented for country {countryCode}");
            }
        }
    }
}