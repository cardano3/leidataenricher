﻿using LeiDataEnricher.Domain.Interface;

namespace LeiDataEnricher.Domain.Strategy
{
    public class GBTransactionCostsCalculator : ITransactionCostsCalculatorStrategy
    {
        public decimal Calculate(decimal notional, decimal rate) =>
            notional * rate - notional;
    }
}