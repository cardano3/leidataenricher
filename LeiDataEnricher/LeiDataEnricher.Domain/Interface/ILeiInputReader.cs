﻿using LeiDataEnricher.Domain.Model;

namespace LeiDataEnricher.Domain.Interface
{
    public interface ILeiInputReader
    {
        LeiRecords ReadLeiData();
    }
}