﻿using LeiDataEnricher.Domain.Model;

namespace LeiDataEnricher.Domain.Interface
{
    public interface ILeiDataEnricherService
    {
        Task<EnrichedLeiRecords> EnrichLeiDataAsync();
    }
}