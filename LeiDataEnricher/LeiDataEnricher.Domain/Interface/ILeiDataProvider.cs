﻿using LeiDataEnricher.Domain.Model;

namespace LeiDataEnricher.Domain.Interface
{
    public interface ILeiDataProvider
    {
        Task<LeiProviderMetadata> GetLeiMetadataAsync(string[] leis);
    }
}