﻿using LeiDataEnricher.Domain.Model;

namespace LeiDataEnricher.Domain.Interface
{
    public interface IEnrichedLeiDataFactory
    {
        EnrichedLeiData BuildEnrichedLeiData(LeiData record, LeiMetadata? metadata);
    }
}