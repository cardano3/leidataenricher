﻿namespace LeiDataEnricher.Domain.Interface
{
    public interface ITransactionCostsCalculatorStrategy
    {
        decimal Calculate(decimal notional, decimal rate);
    }
}