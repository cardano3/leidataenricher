﻿namespace LeiDataEnricher.Domain.Interface
{
    public interface IGetTransactionCostsCalculatorStrategy
    {
        ITransactionCostsCalculatorStrategy GetStrategy(string country);
    }
}