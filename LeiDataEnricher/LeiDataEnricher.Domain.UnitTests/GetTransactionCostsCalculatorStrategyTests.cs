using AutoFixture;
using AutoFixture.AutoFakeItEasy;
using FakeItEasy;
using LeiDataEnricher.Domain.Exceptions;
using LeiDataEnricher.Domain.Strategy;
using NUnit.Framework;
using System.Collections.Specialized;

namespace LeiDataEnricher.Domain.UnitTests
{
    public class Tests
    {
        private IFixture _fixture = null!;
        private GetTransactionCostsCalculatorStragety _sut = null!;

        [SetUp]
        public void Setup()
        {
            _fixture = new Fixture().Customize(new AutoFakeItEasyCustomization());

            _sut = _fixture.Create<GetTransactionCostsCalculatorStragety>();
        }

        [TestCase("gb")]
        [TestCase("nl")]
        public void GetCorrespondingStrategy_BasedOnCountry(string countryCode)
        {
            // act
            var result = _sut.GetStrategy(countryCode);

            // assert
            switch (countryCode.ToLower())
            {
                case "gb":
                    Assert.IsAssignableFrom(typeof(GBTransactionCostsCalculator), result);
                    break;
                case "nl":
                    Assert.IsAssignableFrom(typeof(NLTransactionCostsCalculator), result);
                    break;
                default:
                    break;
            }
        }

        [TestCase("ro")]
        [TestCase("ie")]
        [TestCase("de")]
        [TestCase(null)]
        [TestCase("")]
        public void GetCorrespondingStrategy_ThrowsNoCalculationStrategyException(string countryCode)
        {
            try
            {
                // act
                var result = _sut.GetStrategy(countryCode);
            }
            catch (NoCalculationStrategyException)
            {
                Assert.Pass();
            }
        }
    }
}