﻿using AutoFixture;
using AutoFixture.AutoFakeItEasy;
using FakeItEasy;
using LeiDataEnricher.Domain.Exceptions;
using LeiDataEnricher.Domain.Interface;
using LeiDataEnricher.Domain.Model;
using LeiDataEnricher.Domain.Service;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeiDataEnricher.Domain.UnitTests
{
    public class LeiDataEnricherServiceTests
    {
        private IFixture _fixture = null!;
        private ILeiDataProvider _dataProvider = null!;
        private ILeiInputReader _inputReader = null!;
        private IEnrichedLeiDataFactory _enrichedLeiDataFactory = null!;
        private LeiDataEnricherService _sut = null!;

        [SetUp]
        public void Setup()
        {
            _fixture = new Fixture().Customize(new AutoFakeItEasyCustomization());
            _dataProvider = _fixture.Freeze<ILeiDataProvider>();
            _inputReader = _fixture.Freeze<ILeiInputReader>();
            _enrichedLeiDataFactory = _fixture.Freeze<IEnrichedLeiDataFactory>();

            _sut = _fixture.Create<LeiDataEnricherService>();
        }

        [Test]
        public async Task GetEnrichedLeiData_WithSuccessful_Records()
        {
            // arrange
            A.CallTo(() => _inputReader.ReadLeiData()).Returns(ImportedLeiRecordsNoFailedRows);
            A.CallTo(() => _dataProvider.GetLeiMetadataAsync(A<string[]>._)).Returns(LeiProviderMetadata);
            A.CallTo(() => _enrichedLeiDataFactory.BuildEnrichedLeiData(A<LeiData>._, A<LeiMetadata>._)).Returns(_fixture.Create<EnrichedLeiData>());

            // act
            var result = await _sut.EnrichLeiDataAsync();

            // assert
            Assert.IsTrue(result.SuccessfulRecords.Count == LeiProviderMetadata.Data.Length);
            Assert.IsTrue(result.FailedRecords.Count == 0);
            A.CallTo(() => _inputReader.ReadLeiData()).MustHaveHappened();
            A.CallTo(() => _dataProvider.GetLeiMetadataAsync(A<string[]>._)).MustHaveHappened();
            A.CallTo(() => _enrichedLeiDataFactory.BuildEnrichedLeiData(A<LeiData>._, A<LeiMetadata>._)).MustHaveHappened();
        }

        [Test]
        public async Task GetEnrichedLeiData_WithSuccessful_And_FailedImported_Records()
        {
            // arrange
            A.CallTo(() => _inputReader.ReadLeiData()).Returns(ImportedLeiRecordsWithFailedRows);
            A.CallTo(() => _dataProvider.GetLeiMetadataAsync(A<string[]>._)).Returns(LeiProviderMetadata);
            A.CallTo(() => _enrichedLeiDataFactory.BuildEnrichedLeiData(A<LeiData>._, A<LeiMetadata>._)).Returns(_fixture.Create<EnrichedLeiData>());

            // act
            var result = await _sut.EnrichLeiDataAsync();

            // assert
            Assert.IsTrue(result.SuccessfulRecords.Count == LeiProviderMetadata.Data.Count());
            Assert.IsTrue(result.FailedRecords.Count == ImportedLeiRecordsWithFailedRows.FailedRecords.Count);
            Assert.IsTrue(result.FailedRecords.Count > 0);
            A.CallTo(() => _inputReader.ReadLeiData()).MustHaveHappened();
            A.CallTo(() => _dataProvider.GetLeiMetadataAsync(A<string[]>._)).MustHaveHappened();
            A.CallTo(() => _enrichedLeiDataFactory.BuildEnrichedLeiData(A<LeiData>._, A<LeiMetadata>._)).MustHaveHappened();
        }

        [Test]
        public async Task GetEnrichedLeiData_WithSuccessful_And_MissingMetadata_Records()
        {
            // arrange
            var leiProviderMetadata = new LeiProviderMetadata { Data = new LeiMetadata[] { new() { Id = "lei1" } } };

            A.CallTo(() => _inputReader.ReadLeiData()).Returns(ImportedLeiRecordsNoFailedRows);
            A.CallTo(() => _dataProvider.GetLeiMetadataAsync(A<string[]>._)).Returns(leiProviderMetadata);
            A.CallTo(() => _enrichedLeiDataFactory.BuildEnrichedLeiData(A<LeiData>._, A<LeiMetadata>._)).Returns(_fixture.Create<EnrichedLeiData>());

            // act
            var result = await _sut.EnrichLeiDataAsync();

            // assert
            Assert.IsTrue(result.SuccessfulRecords.Count != LeiProviderMetadata.Data.Length);
            Assert.IsTrue(result.FailedRecords.Count > 0);
            A.CallTo(() => _inputReader.ReadLeiData()).MustHaveHappened();
            A.CallTo(() => _dataProvider.GetLeiMetadataAsync(A<string[]>._)).MustHaveHappened();
            A.CallTo(() => _enrichedLeiDataFactory.BuildEnrichedLeiData(A<LeiData>._, A<LeiMetadata>._)).MustHaveHappened();
        }

        [Test]
        public async Task GetEnrichedLeiData_WithSuccessful_FailedImported_And_MissingMetadata_Records()
        {
            // arrange
            var leiProviderMetadata = new LeiProviderMetadata { Data = new LeiMetadata[] { new() { Id = "lei1" } } };

            A.CallTo(() => _inputReader.ReadLeiData()).Returns(ImportedLeiRecordsWithFailedRows);
            A.CallTo(() => _dataProvider.GetLeiMetadataAsync(A<string[]>._)).Returns(leiProviderMetadata);
            A.CallTo(() => _enrichedLeiDataFactory.BuildEnrichedLeiData(A<LeiData>._, A<LeiMetadata>._)).Returns(_fixture.Create<EnrichedLeiData>());

            // act
            var result = await _sut.EnrichLeiDataAsync();

            // assert
            Assert.IsTrue(result.SuccessfulRecords.Count != LeiProviderMetadata.Data.Length);
            Assert.IsTrue(result.FailedRecords.Count >= 2); 
            A.CallTo(() => _inputReader.ReadLeiData()).MustHaveHappened();
            A.CallTo(() => _dataProvider.GetLeiMetadataAsync(A<string[]>._)).MustHaveHappened();
            A.CallTo(() => _enrichedLeiDataFactory.BuildEnrichedLeiData(A<LeiData>._, A<LeiMetadata>._)).MustHaveHappened();
        }

        [Test]
        public async Task GetEnrichedLeiData_With_No_SuccessfulRecords_Throws_LeiInputDataReaderException()
        {
            // arrange
            A.CallTo(() => _inputReader.ReadLeiData()).Throws<LeiInputDataReaderException>(e => new LeiInputDataReaderException());

            // act
            var result = await _sut.EnrichLeiDataAsync();

            // assert
            Assert.IsTrue(result.SuccessfulRecords.Count == 0);
            Assert.IsTrue(result.FailedRecords.Count > 0);
            A.CallTo(() => _inputReader.ReadLeiData()).MustHaveHappened();
            A.CallTo(() => _dataProvider.GetLeiMetadataAsync(A<string[]>._)).MustNotHaveHappened();
            A.CallTo(() => _enrichedLeiDataFactory.BuildEnrichedLeiData(A<LeiData>._, A<LeiMetadata>._)).MustNotHaveHappened();
        }

        [Test]
        public async Task GetEnrichedLeiData_With_No_SuccessfulRecords_Throws_GleifDataProviderException()
        {
            // arrange
            A.CallTo(() => _inputReader.ReadLeiData()).Returns(ImportedLeiRecordsNoFailedRows);
            A.CallTo(() => _dataProvider.GetLeiMetadataAsync(A<string[]>._))
                .Throws<GleifDataProviderException>(e => new GleifDataProviderException());

            // act
            var result = await _sut.EnrichLeiDataAsync();

            // assert
            Assert.IsTrue(result.SuccessfulRecords.Count == 0);
            Assert.IsTrue(result.FailedRecords.Count > 0);
            A.CallTo(() => _inputReader.ReadLeiData()).MustHaveHappened();
            A.CallTo(() => _dataProvider.GetLeiMetadataAsync(A<string[]>._)).MustHaveHappened();
            A.CallTo(() => _enrichedLeiDataFactory.BuildEnrichedLeiData(A<LeiData>._, A<LeiMetadata>._)).MustNotHaveHappened();
        }

        [Test]
        public async Task GetEnrichedLeiData_With_No_SuccessfulRecords_Throws_NoCalculationStrategyException()
        {
            // arrange
            A.CallTo(() => _inputReader.ReadLeiData()).Returns(ImportedLeiRecordsWithFailedRows);
            A.CallTo(() => _dataProvider.GetLeiMetadataAsync(A<string[]>._)).Returns(LeiProviderMetadata);
            A.CallTo(() => _enrichedLeiDataFactory.BuildEnrichedLeiData(A<LeiData>._, A<LeiMetadata>._))
                .Throws<NoCalculationStrategyException>(ex => new NoCalculationStrategyException());

            // act
            var result = await _sut.EnrichLeiDataAsync();

            // assert
            Assert.IsTrue(result.SuccessfulRecords.Count == 0);
            Assert.IsTrue(result.FailedRecords.Count > 0);
            A.CallTo(() => _inputReader.ReadLeiData()).MustHaveHappened();
            A.CallTo(() => _dataProvider.GetLeiMetadataAsync(A<string[]>._)).MustHaveHappened();
            A.CallTo(() => _enrichedLeiDataFactory.BuildEnrichedLeiData(A<LeiData>._, A<LeiMetadata>._)).MustHaveHappenedOnceOrMore();
        }

        [Test]
        public async Task GetEnrichedLeiData_With_No_SuccessfulRecords_Throws_Exception()
        {
            // arrange
            A.CallTo(() => _inputReader.ReadLeiData()).Throws<Exception>(ex => new Exception());

            // act
            var result = await _sut.EnrichLeiDataAsync();

            // assert
            Assert.IsTrue(result.SuccessfulRecords.Count == 0);
            Assert.IsTrue(result.FailedRecords.Count > 0);
            A.CallTo(() => _dataProvider.GetLeiMetadataAsync(A<string[]>._)).MustNotHaveHappened();
            A.CallTo(() => _enrichedLeiDataFactory.BuildEnrichedLeiData(A<LeiData>._, A<LeiMetadata>._)).MustNotHaveHappened();
        }

        private static LeiRecords ImportedLeiRecordsNoFailedRows => new()
        {
            SuccessfulRecords = new List<LeiData>
            {
                new()  { Lei = "lei1", Notional = 10, Rate = 0.5M },
                new()  { Lei = "lei2", Notional = 20, Rate = 0.15M }
            },
        };

        private static LeiRecords ImportedLeiRecordsWithFailedRows => new()
        {
            SuccessfulRecords = new List<LeiData>
            {
                new()  { Lei = "lei1", Notional = 10, Rate = 0.5M },
                new()  { Lei = "lei2", Notional = 20, Rate = 0.15M }
            },
            FailedRecords = new List<FailedLeiRecord> { new() }
        };

        private static LeiProviderMetadata LeiProviderMetadata => new()
        {
            Data = new LeiMetadata[]
            {
                new() { Id = "lei1" },
                new() { Id = "lei2" },
            }
        };
    }
}