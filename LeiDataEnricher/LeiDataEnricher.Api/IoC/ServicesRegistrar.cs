﻿using LeiDataEnricher.Api.Middleware;
using LeiDataEnricher.Domain.Factory;
using LeiDataEnricher.Domain.Interface;
using LeiDataEnricher.Domain.Service;
using LeiDataEnricher.Domain.Strategy;
using LeiDataEnricher.Infrastructure;
using LeiDataProvider.Interface;

namespace LeiDataEnricher.Api.IoC
{
    public static class ServicesRegistrar
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<ExceptionLoggerMiddleware>();
            services.AddScoped<ILeiDataEnricherService, LeiDataEnricherService>();
            services.AddScoped<ILeiDataProvider, GleifDataProvider>();
            services.AddScoped<ILeiInputReader, LeiInputReader>();
            services.AddScoped<IEnrichedLeiDataFactory, EnrichedLeiDataFactory>();
            services.AddScoped<IGetTransactionCostsCalculatorStrategy, GetTransactionCostsCalculatorStragety>();
        }
    }
}