﻿using log4net;

namespace LeiDataEnricher.Api.Middleware
{
    public class ExceptionLoggerMiddleware : IMiddleware
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ExceptionLoggerMiddleware));

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                Log.Error("There was an error with this request.", ex);
                throw;
            }
        }
    }
}