using LeiDataEnricher.Api.IoC;
using LeiDataEnricher.Api.Logger;
using LeiDataEnricher.Api.Middleware;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

var log4NetConfigFile = "log4net.config";
LoggerConfiguration.Enable(log4NetConfigFile);
builder.Services.AddLogging(x => x.ClearProviders());

// Add services to the container.
builder.Services.RegisterServices();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.UseMiddleware<ExceptionLoggerMiddleware>();

app.Run();