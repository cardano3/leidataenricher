using LeiDataEnricher.Domain.Interface;
using Microsoft.AspNetCore.Mvc;

namespace LeiDataProvider.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LeiDataController : ControllerBase
    {
        private readonly ILeiDataEnricherService _dataEnricherService;

        public LeiDataController(ILeiDataEnricherService dataEnricherService)
        {
            _dataEnricherService = dataEnricherService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _dataEnricherService.EnrichLeiDataAsync());
        }
    }
}