﻿using log4net;
using log4net.Config;
using System.Reflection;
using System.Xml;

namespace LeiDataEnricher.Api.Logger
{
    public static class LoggerConfiguration
    {
        public static void Enable(string configFileName)
        {
            var log4NetConfig = new XmlDocument();
            log4NetConfig.Load(File.OpenRead(configFileName));

            var repo = LogManager.CreateRepository(
                Assembly.GetEntryAssembly(),
                typeof(log4net.Repository.Hierarchy.Hierarchy)
            );
            XmlConfigurator.Configure(repo, log4NetConfig["log4net"]);
        }
    }
}