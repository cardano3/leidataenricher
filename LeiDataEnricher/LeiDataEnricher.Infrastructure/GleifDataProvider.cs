﻿using LeiDataEnricher.Domain.Exceptions;
using LeiDataEnricher.Domain.Interface;
using LeiDataEnricher.Domain.Model;
using log4net;
using System.Text.Json;

namespace LeiDataProvider.Interface
{
    public class GleifDataProvider : ILeiDataProvider
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(GleifDataProvider));

        private const string GleifBaseUrl = "https://api.gleif.org/api/v1/";
        private const string LeiRecordsPath = "lei-records?filter[lei]=";

        public async Task<LeiProviderMetadata> GetLeiMetadataAsync(string[] leis)
        {
            using var httpClient = new HttpClient
            {
                BaseAddress = new Uri(GleifBaseUrl)
            };

            var joinedLeis = string.Join(',', leis);

            Log.Debug($"Getting Lei Metadata for [{joinedLeis}] from Gleif...");

            var response = await httpClient.GetAsync($"{LeiRecordsPath}{joinedLeis}");

            if (!response.IsSuccessStatusCode)
            {
                Log.Error($"Got {response.StatusCode} from GLEIF Api. Won't be able to get lei metadata");
                throw new GleifDataProviderException($"Something went wrong trying to get Lei metadata for {joinedLeis}.");
            }

            var json = await response.Content.ReadAsStringAsync();
            var leisMetadata = JsonSerializer.Deserialize<LeiProviderMetadata>(json, new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase, WriteIndented = true });

            return leisMetadata!;
        }
    }
}