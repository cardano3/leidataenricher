﻿using ExcelDataReader;
using LeiDataEnricher.Domain.Exceptions;
using LeiDataEnricher.Domain.Interface;
using LeiDataEnricher.Domain.Model;
using log4net;

namespace LeiDataEnricher.Infrastructure
{
    public class LeiInputReader : ILeiInputReader
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(LeiInputReader));

        public LeiRecords ReadLeiData()
        {
            Log.Debug("Reading CSV file...");

            try
            {
                var excelFilePath = Path.Combine("input_dataset.csv");
                FileStream fStream = File.Open(excelFilePath, FileMode.Open, FileAccess.Read);

                var result = new LeiRecords();
                using var reader = ExcelReaderFactory.CreateCsvReader(fStream);
                do
                {
                    int i = 0;
                    while (reader.Read())
                    {
                        if (i != 0)
                        {
                            try
                            {
                                LeiData leiData = new()
                                {
                                    TransactionUti = reader.GetString(0),
                                    Isin = reader.GetString(1),
                                    Notional = Convert.ToDecimal(reader.GetString(2)),
                                    Currency = reader.GetString(3),
                                    TransactionType = reader.GetString(4),
                                    TransactionDateTime = Convert.ToDateTime(reader.GetString(5)),
                                    Rate = Convert.ToDecimal(reader.GetString(6)),
                                    Lei = reader.GetString(7),
                                };

                                result.SuccessfulRecords.Add(leiData);
                            }
                            catch (Exception ex)
                            {
                                result.FailedRecords.Add(new FailedLeiRecord
                                {
                                    RecordError = ex.Message
                                });
                                i++;
                                continue;
                            }
                        }
                        i++;
                    }
                } while (reader.NextResult());

                return result;
            }
            catch (Exception ex)
            {
                Log.Error("There was an error reading data from the csv.", ex);
                throw new LeiInputDataReaderException("Couldn't read input data from csv.");
            }
        }
    }
}