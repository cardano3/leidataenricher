I put the input CSV inside LeiDataEnricher.Api folder. I added two more rows at the bottom just to test how the application behaves when:
- a LEI can't be found in gleif api
- a LEI country is something other than GB or NL

I realize there's room for improvement, but I hope I got the right balance of simplicity (or complexity).
